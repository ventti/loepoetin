#!/usr/bin/python
# -*- coding: utf-8 -*-
vocals = ["a","e","i","o","u","y","ä","ö"]
consonants = ["b","c","d","f","g","h","j","k","l","m","n","p","q","r","s","t","v","x","z"]
pauses = [",",".","!","?"," "]
modifiers = ["^","_"]
phonemes = {
    "a": {
        "freqhi": 0x30,  # taajuus hex
        "freqlo": 0x00,  # taajuus hex
        "ad": 0x10,  # Attack/Decay
        "sr": 0x20,  # Sustain/Release
        "len": 8,  # length in frames
        "wave": 0x41,  # waveform
        "dc": 0x80,  # dutycycle
    },
    "s": {
        "freqhi": 0x30,  # taajuus hex
        "freqlo": 0x00,  # taajuus hex
        "ad": 0x10,  # Attack/Decay
        "sr": 0x20,  # Sustain/Release
        "len": 8,  # length in frames
        "wave": 0x81,  # waveform
        "dc": 0x80,  # dutycycle
    },
    "t": {
        "freqhi": 0x30,  # taajuus hex
        "freqlo": 0x00,  # taajuus hex
        "ad": 0x01,  # Attack/Decay
        "sr": 0x00,  # Sustain/Release
        "len": 8,  # length in frames
        "wave": 0x81,  # waveform
        "dc": 0x80,  # dutycycle
    },

    "ss": {
        "freqhi": 0x30,  # taajuus hex
        "freqlo": 0x00,  # taajuus hex
        "ad": 0x10,  # Attack/Decay
        "sr": 0x20,  # Sustain/Release
        "len": 16,  # length in frames
        "wave": 0x81,  # waveform
        "dc": 0x80,  # dutycycle
    },
    "aa": {
        "freqhi": 0x30,  # taajuus hex
        "freqlo": 0x00,  # taajuus hex
        "ad": 0x10,  # Attack/Decay
        "sr": 0x20,  # Sustain/Release
        "len": 16,  # length in frames
        "wave": 0x41,  # waveform
        "dc": 0x80,  # dutycycle
    }
}


def is_vocal(phoneme):
    return len(phoneme) == 1 and phoneme in vocals

def is_consonant(phoneme):
    return len(phoneme) == 1 and phoneme in consonants

def is_double_vocal(phoneme):
    return len(phoneme) == 2 and phoneme[0] == phoneme [1] and phoneme[0] in vocals

def is_double_consonant(phoneme):
    return len(phoneme) == 2 and phoneme[0] == phoneme [1] and phoneme[0] in consonants

def is_pause(phoneme):
    return len(phoneme) == 1 and phoneme in pauses

def create_params(phoneme):
    if not phoneme:
        return
    if is_vocal(phoneme):
        default = phonemes.get("a")
    elif is_consonant(phoneme):
        default = phonemes.get("s")
    elif is_double_vocal(phoneme):
        default = phonemes.get("aa")
    elif is_double_consonant(phoneme):
        default = phonemes.get("ss")
    elif is_pause(phoneme):
        default = phonemes.get(".")
    params = phonemes.get(phoneme, default)
    return params

def as_kickass(param_list):
    tables = {}
    for param in param_list:
        for key, val in param.items():
            tables.setdefault(key, []).append(val)
    prevkey = None
    prevlen = None
    for key, val in tables.items():
        #valstr = f"//{key}\n"
        valstr = ""#// loepoetin data\n\n"
        if prevkey:
            valstr += f"* = {prevkey} + {255 - prevlen % 256}\n"
        prevkey = key
        prevlen = len(val)
        valstr += f"{key}:\n"
        print(valstr)
        print(list_as_kickass(val))
#        valstr += ".byte "
#        for n in val:
#            valstr += "${:02x}, ".format(n)
#        valstr = valstr[:-2]  # dirty!
#        print (valstr)

def list_as_kickass(l):
    valstr = ".byte "
    for n in l:
        valstr += "${:02X}, ".format(n)
    #print (valstr[:-2])
    return valstr[:-2]  # dirty!

text = "/a/s/t/i/a/k/aa/pp/i"
#text = "a/pp/i/l/a/n/ /p/a/pp/i/l/a/n/ /a/p/u/p/a/p/i/n/ /p/a/p/u/p/a/t/a"
phoneme_list = []
offset_list = []
for item in text.split("/"):
    # print(f"{item}")
    params = create_params(item)
    try:
        index = phoneme_list.index(params)  # exists?
        # print(f"{item} exists")
        offset_list.append(index)

    except ValueError:
        if params:
            phoneme_list.append(params)
        offset_list.append(len(phoneme_list))

as_kickass(phoneme_list)
print("\n//---------------------------------")
print("// offset sequence")
print("offsets:")
#print (offset_list)
print(list_as_kickass(offset_list))

# FIXME
# find uniques
#
