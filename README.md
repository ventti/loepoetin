# Loepoetin





* "lukee" tekstin pseudopuheella writerin tahdissa, esim. 
	* kirjoitettu teksti ja "puhuttu" teksti ("foneemit") omat erilliset datansa
	* määritellen "foneemeille" staattiset äänet
	* joko pwm tai kohina
	* pitch, dutycycle
	* kesto (framea)

	* Pythonilla määritellään parametrit ja luetaan teksti -> generoidaan sorsa dictistä, esim
	phonemes = {
		"a": {
			"freq": 300,  #taajuus Hz
			"ad": 0x10,  # Attack/Decay
			"sr": 0x20,  # Sustain/Release
			"len": 8,  # length in frames
			"wave": 0x41,  # waveform
			"dc": 0x80,  # dutycycle
		}
	}

	foneemeista muodostetaan lista, ja generoidaan kickass-muodossa parametritaulukot -> samalla indeksilla löytyy joka taulukosta oikeat arvot


	# kirjoitetaan teksti auki
	# generoidaan siitä //-splitattu
	# editoidaan käsin loogisiksi kokonaisuuksiksi
	text = "/s/ää/ /r/a/nn/i/kk/o/a/s/e/m/i/ll/a/ /t/ä/n/ää/n"
	
	# määritellään ensin vakioarvot, iteroidaan tarpeen vaatiessa kunnes "puheesta" saa selvää
	
	splitataan "/" ja haetaan parametrit
	
	for phoneme in text.split("/"):
		if isvocal(phoneme):
			default = 'a'  # 'default_vocal'
		elif is_consonant(phoneme):
			default = 'default_consonant'
		elif is_double_vocal(phoneme):
			default = 'default_double_vocal'
		elif is_double_consonant(phoneme):
			default = 'default_double_consonant'
		else:
			default = 'default_pause'
		params = phonemes.get(phoneme, phonemes[default])
